using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using TMPro;

/// <summary>
/// Clase encargada de gestionar los nombres de los jugadores
/// </summary>
public class UINickname : NetworkBehaviour
{
	[SyncVar]
    public string m_NicknameText;                      

	private TextMeshProUGUI  m_Input;
	public TextMeshPro m_Nickname;

    // Start is called before the first frame update
    void Start()
    {
        m_Input = GameObject.Find("NicknameText").GetComponent<TextMeshProUGUI >();
    }

    // Update is called once per frame
    void Update()
    {
		if(isLocalPlayer)
		{
			CmdChangeNickname(m_Input.text);
		}

		//Change UI
		m_Nickname.text = m_NicknameText;
    }

	/// <summary>
	/// Cambia el nombre del jugador en todos los clientes
	/// </summary>
	/// <param name="newNickname">Nuevo nombre</param>
	[Command]
	public void CmdChangeNickname(string newNickname) 
	{
			m_NicknameText = newNickname;
	}

}
