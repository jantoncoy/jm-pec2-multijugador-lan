using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

/// <summary>
/// Script que contiene la configuracion del lobby al presionar los botones
/// </summary>
public class Lobby : MonoBehaviour 
{
    private NetworkManager manager;
    public string serverIP = "localhost";

    void Awake(){
        manager = FindObjectOfType<NetworkManager>();
    }

    /// <summary>
    /// Crea una partida nueva y se une
    /// </summary>
    public void CreateGame() 
	{
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.StartHost();
            }
        }

        ServerData();
    }

    /// <summary>
    /// Se une a una partida
    /// </summary>
    public void JoinGame() 
	{
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.networkAddress = serverIP;
                manager.StartClient();
            }
        }

        ServerData();
    }

    /// <summary>
    /// Inicia unicamente el servidor
    /// </summary>
    public void CreateServer() 
	{
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.StartServer();
            }
        }

        ServerData();
    }

    /// <summary>
    /// Muestra informacion
    /// </summary>
    private void ServerData() 
	{
		ServerJoined();
        Debug.Log ("Local IP Address: " + GetLocalIP() + "\r\n ------------------------------------------------------------------------");
    }

    /// <summary>
    /// devuelve la ip 
    /// </summary>
    /// <returns></returns>
	private string GetLocalIP() 
	{
		var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
		foreach (var ip in host.AddressList) 
		{
			if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) 
			{
			    return ip.ToString();
			}
		}

		throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }

    /// <summary>
    /// Muestra informacion del servidor
    /// </summary>
	private void ServerJoined()
	{
		if (NetworkServer.active) 
		{
            Debug.Log ("Server IP: " + manager.networkAddress + " / Transport: " + Transport.activeTransport);
        }
        else
		{
            Debug.Log ("Could not join server: " + serverIP);
        }
	}
}
